from lib.markdown2 import Markdown
import sys
import os
import shutil



filename = sys.argv[1]
DIR=os.path.dirname(sys.argv[1])


with open(filename, 'r') as content_file:
    content = content_file.read()

slides=content.split("--SLIDE--")

starting = '<div class="slidecontainer">'
itemTemplate = '<div class="slide">{{{{{slidecontent}}}}}</div>'
ending = '</div>'

MD = Markdown()

output = starting 
for i in slides:
	if i=="": continue # discard emplty slides
	output+=itemTemplate.replace("{{{{{slidecontent}}}}}",MD.convert(i))

output+=ending



with open('./resources/template.html', 'r') as content_file:
    template = content_file.read()
template=template.replace("{{{{{pochani}}}}}",output)


filename=filename.replace("md","html")
filename=filename.replace("markdown","html")

if not os.path.exists(DIR+"/res"):
	os.makedirs(DIR+"/res")

with open(filename, 'w+') as content_file:
    content_file.write(template)

shutil.copy2('./resources/style.css',DIR+'/res/style.css')
shutil.copy2('./resources/pochaslider.min.js',DIR+'/res/pochaslider.min.js')
shutil.copy2('./resources/functions.min.js',DIR+'/res/functions.min.js')
shutil.copy2('./resources/jquery.js',DIR+'/res/jquery.js')
shutil.copy2('./resources/touch.js',DIR+'/res/touch.js')

if len(sys.argv)==3:
	shutil.copy2(sys.argv[2],DIR+'/res/template.css')
else: shutil.copy2('./resources/template.css',DIR+'/res/template.css')

print "yo yo honey singh!"
